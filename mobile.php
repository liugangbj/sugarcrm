<?php
function isMobile(){
	// 如果有HTTP_X_WAP_PROFILE则一定是移动设备
	if(isset($_SERVER['HTTP_X_WAP_PROFILE'])){
		return true;
	}
	
	// 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
	if(isset($_SERVER['HTTP_VIA'])){// 找不到为flase,否则为true
          //return stristr($_SERVER['HTTP_VIA'],"wap")? true : false;
            if(stristr($_SERVER['HTTP_VIA'],"wap")){
                return true;
            }
	}
	
	//判断手机发送的客户端标志,兼容性有待提高
	if(isset($_SERVER['HTTP_USER_AGENT'])){
            $clientkeywords=array(
                                    'nokia',
                                    'sony',
                                    'ericsson',
                                    'mot',
                                    'samsung',
                                    'htc',
                                    'sgh',
                                    'lg',
                                    "lg-",
                                    "lge-",
                                    "lge",
                                    'sharp',
                                    'sie-',
                                    'philips',
                                    'panasonic',
                                    'alcatel',
                                    'lenovo',
                                    'iphone',
                                    'ipod',
                                    'blackberry',
                                    'meizu',
//                                    'android',
                                    'netfront',
                                    'symbian',
                                    'ucweb',
                                    'windowsce',
                                    'palm',
                                    'operamini',
                                    'operamobi',
                                    'openwave',
                                    'nexusone',
                                    'cldc',
                                    'midp',
                                    'wap',
//                                    'mobile'
                                    'MI',
                                    'xiaomi',
                                    'huawei',
                                    'etouch',
                                    'iemobile',
                                    'windows ce',
                                  );
	
            // 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if(preg_match("/(".implode('|',$clientkeywords).")/i",strtolower($_SERVER['HTTP_USER_AGENT']))){
                return true;
            }
	
	}
	
	// 协议法，因为有可能不准确，放到最后判断
	if(isset($_SERVER['HTTP_ACCEPT'])){
            // 如果只支持wml并且不支持html那一定是移动设备;如果支持wml和html但是wml在html之前则是移动设备
            if((strpos($_SERVER['HTTP_ACCEPT'],'vnd.wap.wml')!==false)&&(strpos($_SERVER['HTTP_ACCEPT'],'text/html')===false||(strpos($_SERVER['HTTP_ACCEPT'],'vnd.wap.wml')<strpos($_SERVER['HTTP_ACCEPT'],'text/html'))))
            {
                return true;
            }
	}
	
	return false;
}

$is_mobile = isMobile();
if($is_mobile){
    echo $_SERVER['HTTP_USER_AGENT'] . "<br>";
    echo 'is mobile';
}else{
    echo 'PC';
}

?>

<?php
function userAgent($ua){
 
    $iphone = strstr(strtolower($ua), 'mobile'); //Search for 'mobile' in user-agent (iPhone have that)
    $android = strstr(strtolower($ua), 'android'); //Search for 'android' in user-agent
    $windowsPhone = strstr(strtolower($ua), 'phone'); //Search for 'phone' in user-agent (Windows Phone uses that)
            
    function androidTablet($ua){ //Find out if it is a tablet
        if(strstr(strtolower($ua), 'android') ){//Search for android in user-agent
            if(!strstr(strtolower($ua), 'mobile')){ //If there is no ''mobile' in user-agent (Android have that on their phones, but not tablets)
                return true;
            }
        }
    }
    $androidTablet = androidTablet($ua); //Do androidTablet function
    $ipad = strstr(strtolower($ua), 'ipad'); //Search for iPad in user-agent
      
    if($androidTablet || $ipad){ //If it's a tablet (iPad / Android)
        return 'tablet';
    }
    elseif($iphone && !$ipad || $android && !$androidTablet || $windowsPhone){ //If it's a phone and NOT a tablet
        return 'mobile';
    }
    else{ //If it's not a mobile device
        return 'desktop';
    }    
}

$is_mobile1 = userAgent($_SERVER['HTTP_USER_AGENT']);

echo $is_mobile1;
?>