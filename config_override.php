<?php
/***CONFIGURATOR***/
$sugar_config['verify_client_ip'] = false;
$sugar_config['default_module_favicon'] = false;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['enable_action_menu'] = false;
$sugar_config['stack_trace_errors'] = false;
$sugar_config['developerMode'] = false;
$sugar_config['addAjaxBannedModules'][0] = 'Calls';
$sugar_config['addAjaxBannedModules'][1] = 'Leads';
$sugar_config['addAjaxBannedModules'][2] = 'Bugs';
$sugar_config['addAjaxBannedModules'][3] = 'Meetings';
$sugar_config['addAjaxBannedModules'][4] = 'Opportunities';
$sugar_config['addAjaxBannedModules'][5] = 'Accounts';
$sugar_config['addAjaxBannedModules'][6] = 'Prospects';
$sugar_config['addAjaxBannedModules'][7] = 'Tasks';
$sugar_config['addAjaxBannedModules'][8] = 'Notes';
$sugar_config['addAjaxBannedModules'][9] = 'Cases';
$sugar_config['addAjaxBannedModules'][10] = 'ProspectLists';
$sugar_config['addAjaxBannedModules'][11] = 'Contacts';
/***CONFIGURATOR***/