<?php
// created: 2015-05-09 18:15:16
$unified_search_modules_display = array (
  'Accounts' => 
  array (
    'visible' => true,
  ),
  'Bugs' => 
  array (
    'visible' => false,
  ),
  'Calls' => 
  array (
    'visible' => true,
  ),
  'Campaigns' => 
  array (
    'visible' => false,
  ),
  'Cases' => 
  array (
    'visible' => true,
  ),
  'Contacts' => 
  array (
    'visible' => true,
  ),
  'Documents' => 
  array (
    'visible' => true,
  ),
  'Leads' => 
  array (
    'visible' => true,
  ),
  'Meetings' => 
  array (
    'visible' => true,
  ),
  'Notes' => 
  array (
    'visible' => true,
  ),
  'Opportunities' => 
  array (
    'visible' => true,
  ),
  'Project' => 
  array (
    'visible' => false,
  ),
  'ProjectTask' => 
  array (
    'visible' => false,
  ),
  'ProspectLists' => 
  array (
    'visible' => false,
  ),
  'Prospects' => 
  array (
    'visible' => false,
  ),
  'Tasks' => 
  array (
    'visible' => false,
  ),
);